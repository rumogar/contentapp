#!/usr/bin/env python3

import webApp

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""

class ContentApp(webApp.webApp):

    contenidos = {'/': "<p>Principal page:</p>",
                '/hello': "<p>Ey!</p>",
                '/bye': "<p>Bye!</p>",
                '/seeson': "<p>See you later!</p>",}

    def parse (self, recurso):
        return recurso.split(' ',2)[1]

    def process (self, recurso):
        if recurso in self.contenidos:
            contenido = self.contenidos[recurso]
            page = PAGE.format(contenido=contenido)
            code = "200 Ok"
        else:
            page = PAGE_NOT_FOUND.format(recurso=recurso)
            code = "404 Page not Found"
        return (code, page)

if __name__ == "__main__":
    webApp = ContentApp ("localhost", 1234)
